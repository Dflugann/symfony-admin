<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/posts")
 */
class AdminPostController extends AbstractController
{
    /**
     * @Route("/", name="admin_post")
     */
    public function index()
    {
        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findAll();

        return $this->render('admin_post/index.html.twig', [
            'controller_name' => 'Flavio Santos',
            'posts' => $post
        ]);
    }

    /**
     * @Route("/form", name="admin_post_form")
     */
    public function form(Request $request)
    {
        $form = $this->createForm(FormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $post = $form->getData();

            $post->setCreatedAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));
            $post->setUpdatedAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();
            
            $this->addFlash('success', 'Dados cadastrados com sucesso!!');
            return $this->redirectToRoute('admin_post');

        }
        return $this->render('admin_post/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/update/{id}", name="admin_post_update")
     */
    public function update(Request $request, Post $id)
    {
        $form = $this->createForm(FormType::class, $id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $post = $form->getData();
            
            $post->setUpdatedAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->merge($post);
            $entityManager->flush();
            
            $this->addFlash('success', 'Dados atualizados com sucesso!!');
            return $this->redirectToRoute('admin_post');
        }

        return $this->render('admin_post/update.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="admin_post_delete")
     */
    public function delete(Post $post)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($post);
        $entityManager->flush();
        
        $this->addFlash('success', 'Dados removidos com sucesso!!');
        return $this->redirectToRoute('admin_post');
    }

}
