<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\Common\Annotations\Annotation\Required;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/", name="admin_category")
     */
    public function index()
    {
        $categories = $this->getDoctrine()
                        ->getRepository(Category::class)
                        ->findAll();

        return $this->render('admin_category/index.html.twig', [
            'controller_name'   => 'CategoryController',
            'categories'        =>$categories
        ]);
    }

    /**
     * @Route("/form", name="admin_category_form")
     */
    public function formCategory(Request $request)
    {
        $form = $this->createForm(CategoryType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $category = $form->getData();
            $category->setCreatedAt(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));
            $category->setUpdatedAt(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash('success', 'Categoria cadastrada com sucesso!');
            return $this->redirectToRoute('admin_category');
        }

        return $this->render('admin_category/form.html.twig',[
           'title'    => 'Categoria',
            'form'        => $form->createView()
        ]);

    }

    /**
     * @Route("/update/{id}", name="admin_category_update")
     */
    public function update(Request $request, Category $id)
    {
        $form =  $this->createForm(CategoryType::class, $id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $category = $form->getData();
            $category->setUpdatedAt(new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->merge($category);
            $entityManager->flush();

            $this->addFlash('success', 'Categoria atualizada com sucesso!');
            return $this->redirectToRoute('admin_category');
        }

        return $this->render('admin_category/update.html.twig',[
            'title'     => 'Categoria',
            'form'      =>$form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_category_delete")
     */
    public function delete(Category $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($id);
        $entityManager->flush();

        $this->addFlash('success', 'Categoria excluida com sucesso !');
        return $this->redirectToRoute('admin_category');
    }

}
