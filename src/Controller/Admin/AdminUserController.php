<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/users")
 */
class AdminUserController extends AbstractController
{
    /**
     * @Route("/", name="admin_users")
     */
    public function index()
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('admin_user/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/new", name="admin_user_new")
     */
    public function form(Request $request)
    {
        $user = $this->createForm(UserType::class);
        $user->handleRequest($request);
        $data = $user->getData();
        var_dump($data);exit;


        if ($user->isSubmitted() && $user->isValid()){
            $data = $user->getData();
            var_dump($data);exit;
            $data->setCreatedAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));
            $data->setUpdateAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();
            
            $this->addFlash('success', 'Usuario cadastrado com sucesso!!');
            return $this->redirectToRoute('admin_users');

        }
        return $this->render('admin_user/user.html.twig', [
            'form' => $user->createView()
        ]);
    }

    /**
     * @Route("/update/{id}", name="admin_user_update")
     */
    public function update(Request $request, User $id)
    {
        $user = $this->createForm(UserType::class, $id);
        $user->handleRequest($request);

        if ($user->isSubmitted() && $user->isValid()){
            $data = $user->getData();
            
            $data->setUpdateAt(new \DateTime('now', new \DateTimeZone("America/Sao_Paulo")));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->merge($data);
            $entityManager->flush();
            
            $this->addFlash('success', 'Usuario atualizado com sucesso!!');
            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin_user/update.html.twig', [
            'form' => $user->createView()
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="admin_user_delete")
     */
    public function delete(User $user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();
        
        $this->addFlash('success', 'Usuario removido com sucesso!!');
        return $this->redirectToRoute('admin_users');
    }

}
