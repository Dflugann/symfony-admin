<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $post = $this->getDoctrine()
                ->getRepository(Post::class)
                ->findAll();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'Hello Word, this is my blog',
        ]);
    }
}
